package tests;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class FacturasTest {
	static Factura facturaPrueba;
	Factura esperado;
	Factura actual;
	static GestorContabilidad gestorPrueba;
	static Cliente cliente;
	
	@BeforeClass
	public static void prepararClasePruebas() {
		gestorPrueba = new GestorContabilidad();
		cliente = new Cliente("Marian", "3131Z", LocalDate.parse("2011-03-05"));
		gestorPrueba.getListaClientes().add(cliente);
	}
	
	@Before
	public void iniciar() {
		gestorPrueba.getListaFacturas().clear();
	}
	
	@Test
	public void testCalcularPrecioProducto1() {
		facturaPrueba.setCantidad(3);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado = 7.5F;
		float actual = facturaPrueba.calcularPrecioTotal();		
		
		assertEquals(esperado, actual);
	}
	
	@Test
	public void testCalcularPrecioCantidadCero() {
		facturaPrueba.setCantidad(0);
		facturaPrueba.setPrecioUnidad(2.5F);
		
		float esperado = 0;
		float actual = facturaPrueba.calcularPrecioTotal();		
		
		assertEquals(esperado, actual);
	}
	
	@Test 
	public void testCalcularPrecioConPrecioNegativo() {
		Factura facturaPrueba = new Factura(34, LocalDate.now(), "Manis", -5, 2, 0);
		facturaPrueba.getListaFacturas().add(facturaPrueba);
		
		float esperado = -5;
		float actual = facturaPrueba.calcularPrecioTotal();		
		
		assertEquals(esperado, actual);
	}
	
	@Test
	public void comprobarFacturaExistente() {
		esperado= new Factura(983,LocalDate.now(), "Manis", 34, 5, 3);
		facturaPrueba.getListaFacturas().add(esperado);
		actual= facturaPrueba.buscarFactura(983);
		
		assertEquals(esperado, actual);
	}
	
	@Test
	public void comprobarFacturaInexistente() {
		esperado= new Factura(0,null, null, 0, 0, 0);
		facturaPrueba.getListaFacturas().add(esperado);
		Factura esperado = facturaPrueba.buscarFactura(0);
		
		assertNull(esperado);		
	}
	
	@Test 
	public void testAltaVariasFacturas() {
		Factura nuevaFactura = new Factura(323,LocalDate.parse("2017-2-6"), "Fles", 3, 12, 6);
		facturaPrueba.altaFactura(nuevaFactura);
		
		nuevaFactura = new Factura(232,LocalDate.parse("2015-3-12"), "Fresh", 6, 11, 8);
		facturaPrueba.altaFactura(nuevaFactura);
		
		boolean actual = facturaPrueba.getListaFacturas().contains(nuevaFactura);
		
		assertTrue(actual);
	}
	
	@Test 
	public void eliminarFacturaConCodigoCero() {
		esperado= new Factura(0,LocalDate.now(), "Manis", 34, 5, 3);
		facturaPrueba.getListaFacturas().add(esperado);
		
		gestorPrueba.eliminarFactura(0);
		boolean result = gestorPrueba.getListaFacturas().contains(esperado);
		assertTrue(result);
	}
	
	@Test 
	public void eliminarFacturaConCodigoValido() {
		esperado= new Factura(6,LocalDate.now(), "Manis", 34, 5, 3);
		gestorPrueba.getListaFacturas().add(esperado);
		gestorPrueba.eliminarFactura(6);
		boolean result = gestorPrueba.getListaFacturas().contains(esperado);
		assertFalse(result);
	}
	

	@Test
	public void testFacturaMasCaraListaVacia() {
		Factura actual = gestorPrueba.facturaMasCara(0);

		assertNull(actual);
	}


	@Test
	public void testFacturaMasCaraCon1Factura() {
		Factura unaFactura = new Factura("526253", LocalDate.parse("2018-05-11"), "ps2", 2.5f, 10, cliente);
		gestorPrueba.getListaFacturas().add(unaFactura);
		
		double esperado=2.5;
		Factura actual = gestorPrueba.facturaMasCara((int) 2.5);

		assertSame(esperado, actual);
	}
	
}
