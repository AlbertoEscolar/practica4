package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import clases.Cliente;
import clases.Factura;
import clases.GestorContabilidad;

class ClientesTest {

	static GestorContabilidad gestorPruebas;
	Cliente otroCliente;
	Cliente actual;

	@BeforeClass
	public static void inicializarClase() {
		gestorPruebas = new GestorContabilidad();
	}

	@Before
	public static void antesDeCada() {
		gestorPruebas.getListaCliente().clear();
	}

	@Test
	public void testbuscarClienteInexistenteSinClientes() {
		String dni = "2345234";
		Cliente actual = gestorPruebas.buscarCliente(dni);
		assertNull(actual);
	}

	@Test
	public void buscarClienteInexistenteConCliente() {
		Cliente unCliente = new Cliente("23542D", "Julian", LocalDate.parse("2000-02-08"));
		gestorPruebas.getListaCliente().add(unCliente);
		
		actual = gestorPruebas.buscarCliente("24524P");	
		
		assertNull(actual);
	}

	@Test
	public void testBuscarClienteExistente() {
		// Busca un cliente que si exista, el resultado debe ser el mismo objeto
		otroCliente = new Cliente("1234F", "Fernando", LocalDate.now());
		actual = gestorPruebas.buscarCliente("1234F");

		assertSame(otroCliente, actual);
	}

	@Test
	public void testBuscarClienteInexistenteSinClientes() {
		String dni = "64F";

		Cliente actual = gestorPruebas.buscarCliente(dni);

		assertNull(actual);
	}

	@Test
	public void testBuscarClienteHabiendoVariosClientes() {
		otroCliente = new Cliente("34567F", "Maria", LocalDate.parse(""));
		gestorPruebas.getListaCliente().add(otroCliente);
		String dniABuscar = "12345678L";
		otroCliente = new Cliente(dniABuscar, "Pedro", LocalDate.parse("1995-07-02"));
		gestorPruebas.getListaCliente().add(otroCliente);

		actual = gestorPruebas.buscarCliente(dniABuscar);

		assertNotNull(actual);
	}

	@Test
	public void testAltaPrimerCliente() {
		gestorPruebas.getListaClientes().clear();

		Cliente nuevoCliente = new Cliente("123456", "Pablo", LocalDate.parse("2018-5-5"));
		gestorPruebas.altaCliente(nuevoCliente);

		boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);

		assertTrue(actual);
	}

	@Test
	public void testAltaVariosClientes() {
		// A�ado dos clientes y compruebo que el segundo se ha a�adido
		Cliente nuevoCliente = new Cliente("262L", "Juan", LocalDate.parse("2017-2-5"));
		gestorPruebas.altaCliente(nuevoCliente);

		nuevoCliente = new Cliente("246262L", "Maria", LocalDate.parse("2016-4-5"));
		gestorPruebas.altaCliente(nuevoCliente);

		boolean actual = gestorPruebas.getListaClientes().contains(nuevoCliente);

		assertTrue(actual);
	}

	@Test
	public void comprobarClienteMasAntiguoCon1Cliente() {
		Cliente esperado = new Cliente("Juanin", "14122D", LocalDate.parse("2020-02-03"));
		gestorPruebas.getListaClientes().add(esperado);

		Cliente actual = gestorPruebas.clienteMasAntiguo(LocalDate.parse("2020-02-03"));

		assertSame(esperado, actual);
	}

	@Test
	public void comprobarClienteMasAntiguoCon0Cliente() {
		Cliente actual = gestorPruebas.clienteMasAntiguo(LocalDate.parse("2010-01-12"));
		assertNull(actual);
	}

	@Test
	public void comprobarClienteMasAntiguoCon3Clientes() {
		Cliente otro = new Cliente("3143D", "Mar", LocalDate.parse("2010-3-6"));
		gestorPruebas.getListaCliente().add(otro);
		Cliente nuevo = new Cliente("3423423D", "Juan", LocalDate.parse("2000-3-6"));
		gestorPruebas.getListaCliente().add(nuevo);
		otroCliente = new Cliente("34567F", "Maria", LocalDate.parse("1800-3-6"));
		gestorPruebas.getListaCliente().add(otroCliente);

		actual = gestorPruebas.clienteMasAntiguo(LocalDate.parse("1800-3-6"));

		assertEquals(otroCliente, actual);
	}

	@Test
	public void cantidadFacturasSinClientes() {
		gestorPruebas.getListaClientes().clear();

		int actual = gestorPruebas.cantidadFacturasPorCliente("234521C");

		assertNull(actual);
	}

	@Test
	public void cantidadFacturasPorClientes() {
		Cliente unCliente = new Cliente("Lerian", "3134314", LocalDate.parse("2019-03-05"));
		gestorPruebas.getListaClientes().add(unCliente);
		Factura unaFactura = new Factura("526253", LocalDate.parse("2006-05-11"), "et3", 4.5f, 10, unCliente);
		gestorPruebas.getListaFacturas().add(unaFactura);
		Factura otraFactura = new Factura("52625a3", LocalDate.parse("2006-05-11"), "psa2", 2.5f, 10, unCliente);
		gestorPruebas.getListaFacturas().add(otraFactura);

		int result = gestorPruebas.cantidadFacturasPorCliente("3134314");

		assertEquals(result, 2);
	}

	@Test
	public void testEliminarCliente() {
		Cliente unCliente = new Cliente("Marion", "23452524S", LocalDate.parse("2020-03-05"));
		gestorPruebas.getListaClientes().add(unCliente);

		gestorPruebas.eliminarCliente("23452524S");

		assertFalse(gestorPruebas.getListaClientes().contains(unCliente));
	}

	@Test
	public void testFacturasEliminarCliente() {
		Cliente unCliente = new Cliente("Tiron", "1341343", LocalDate.parse("2020-03-05"));
		gestorPruebas.getListaClientes().add(unCliente);
		gestorPruebas.getListaFacturas().clear();
		Factura unaFacturaCliente = new Factura("1341343", LocalDate.parse("2005-05-11"), "fe2", 5.6f, 1, unCliente);
		gestorPruebas.getListaFacturas().add(unaFacturaCliente);

		gestorPruebas.eliminarCliente("1341343");

		assertTrue(gestorPruebas.getListaClientes().contains(unCliente));
	}

	@Test
	public void testAsignarClienteAFacturaInexistente() {
		Cliente unCliente = new Cliente("Pvxxr", "Gvng", LocalDate.parse("2019-03-05"));
		gestorPruebas.getListaClientes().add(unCliente);
		try {
			gestorPruebas.asignarClienteAFactura("Gvng", "");
		} catch (Exception e) {
			fail("no se esperaba una excepcion");
		}

	}

	@Test
	public boolean testAsignarClienteAFactura() {
		Cliente unCliente = new Cliente("Marius", "32343s", LocalDate.parse("2019-03-05"));
		gestorPruebas.getListaClientes().add(unCliente);
		Factura unaFactura = new Factura("42654", LocalDate.parse("2006-05-11"), "4223", 5.5f, 9, null);
		gestorPruebas.getListaFacturas().add(unaFactura);

		gestorPruebas.asignarClienteAFactura("32343s", "42654");

			if((gestorPruebas.getListaFacturas().get(
						gestorPruebas.getListaFacturas().indexOf(unaFactura)
						).getCliente() == null
				|| !gestorPruebas.getListaFacturas().get(
								gestorPruebas.getListaFacturas().indexOf(unaFactura)
								).getCliente().equals(unCliente)))
		return true;		
		else {
			return false;
		}
	}
	
	@Test
	public void testAsignarClienteAFacturaInexistenteClienteExistente() {
		try {
			gestorPruebas.asignarClienteAFactura("", "");
		} catch (Exception e) {
			fail("no se esperaba una excepcion");
	}
	}
	
}
