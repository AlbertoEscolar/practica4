package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Factura {

	private int codigoFactura;
	private LocalDate fecha;
	private String nombreProducto;
	private float precioUnidad;
	private int cantidad;
	private Cliente cliente;
	
	public Factura(int codigoFactura, LocalDate fecha, String nombreProducto, int precioUnidad, int cantidad, Cliente cliente) {
		this.codigoFactura=codigoFactura;
		this.fecha=fecha;
		this.nombreProducto=nombreProducto;
		this.precioUnidad=precioUnidad;
		this.cantidad=cantidad;
		this.cliente=cliente;
	}
	

	public Factura(String string, LocalDate parse, String nombreProducto2, float f, int cantidad2, Cliente cliente2) {
		// TODO Auto-generated constructor stub
	}


	public Factura(int codigoFactura2, LocalDate parse, String nombreProducto2, int precioUnidad2, int cantidad2,
			int i) {
		// TODO Auto-generated constructor stub
	}


	public float calcularPrecioTotal() {
		return (cantidad*precioUnidad);
	}

	public int getCodigoFactura() {
		return codigoFactura;
	}

	public void setCodigoFactura(int codigoFactura) {
		this.codigoFactura = codigoFactura;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public String getNombreProducto() {
		return nombreProducto;
	}

	public void setNombreProducto(String nombreProducto) {
		this.nombreProducto = nombreProducto;
	}

	public float getPrecioUnidad() {
		return precioUnidad;
	}

	public void setPrecioUnidad(float precioUnidad) {
		this.precioUnidad = precioUnidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	ArrayList<Factura> listaFacturas;
	
	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}


	public Factura buscarFactura(int codigoFactura) {	
		for (Factura factura : listaFacturas){
			if (factura.getCodigoFactura()==codigoFactura) {
				return factura;
			}
		}	
		return null;
	}
	
	public void altaFactura(Factura nuevaFactura) {
		Factura esperado = null;
		for (Factura factura: listaFacturas) {
			if (factura.getCodigoFactura()==codigoFactura) {
			System.out.println("Error, el codigo de factura est� repetido");
			}
			else {
			esperado = new Factura (2334 , LocalDate.parse(""), "Actiones", 45 , 120 , 5);
			}
		listaFacturas.add(esperado);
		}
		
	}


	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}


}
