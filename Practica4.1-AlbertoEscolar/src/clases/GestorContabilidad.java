package clases;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

public class GestorContabilidad {

	ArrayList<Factura> listaFacturas;
	
	ArrayList<Cliente> listaClientes;
	
	
	public Cliente buscarCliente(String dni) {
		for (Cliente cliente : listaClientes)
			if (cliente.getDni().equals(dni)) {
				return cliente;
			}
		return null;
	}
	
	public Factura buscarFactura(int codigoFactura) {	
		for (Factura factura : listaFacturas){
			if (factura.getCodigoFactura()==codigoFactura) {
				return factura;
			}
		}	
		return null;
	}
	
	
	public void altaCliente(Cliente nuevoCliente) {
		Cliente esperado = null;
		for (Cliente cliente: listaClientes) {
			if (nuevoCliente.equals(listaClientes)) {
			System.out.println("Error, El dni ya existe en uno de los clientes");
			}
			else {
			esperado = new Cliente ("34567F" , "Juancho", LocalDate.parse(""));	
			}
		listaClientes.add(esperado);
		}
	}
	
	public void altaFactura(int codigoFactura) {
		Factura esperado = null;
		for (Factura factura: listaFacturas) {
			if (factura.getCodigoFactura()==codigoFactura) {
			System.out.println("Error, el codigo de factura est� repetido");
			}
			else {
			esperado = new Factura (2334 , LocalDate.parse(""), "Actiones", 45 , 120 , 5);
			}
		listaFacturas.add(esperado);
		}
	}
	
	
	public Cliente clienteMasAntiguo(LocalDate fechaAlta) {
		Cliente esperado = null;
		for (Cliente cliente: listaClientes) {
			if (cliente.getFechaAlta().isBefore(fechaAlta)) {
				cliente=esperado;
			}
			else {
				return null;
			}
		}
		return esperado;
	}
	
	public Factura facturaMasCara(int codigoFactura){
		Factura esperada = null;
		for (Factura factura: listaFacturas) {
		if (factura.getCodigoFactura()>codigoFactura) {
			factura=esperada;
		}
		else {
			return null;
		}
		}
		return esperada;
	}
	
	public float calcularFacturacionAnual(int a�o) {
		for (Factura factura: listaFacturas) {
			if (a�o==factura.getFecha().getYear()) {
			return factura.getCantidad()*factura.getPrecioUnidad();
			}
			else {
			return -1;	
			}
		}
		return -1;
	}
	
	public void asignarClienteAFactura(String dni, String codigoFactura) {
	Factura nuevo;
	for (Factura factura: listaFacturas) {
		//if (factura.getCliente()
		
	}
	
	}
	
	public int cantidadFacturasPorCliente(String dni) {
		int sumaFacturas=0;
		for (Cliente cliente: listaClientes) {
			if (cliente.getDni().equalsIgnoreCase(dni)) {
				for (Factura factura: listaFacturas) {
					sumaFacturas=sumaFacturas+factura.getCodigoFactura();
				}
			}
			else {
				System.out.println("No coincide el dni");
			}
		}
		return sumaFacturas;
	}
	
	
	public void eliminarFactura(int codigo) {
		for (Factura factura: listaFacturas) {
			if (factura.getCodigoFactura()==codigo) {
			listaFacturas.remove(listaFacturas.remove(factura));
			}
		}
	}
	
	public void eliminarCliente(String dni) {
		for (Cliente cliente: listaClientes) {
			if (cliente.getDni()==dni) {
			listaClientes.remove(listaClientes.remove(cliente));
			}
		}
	}
	
	
	public void setListaFacturas(ArrayList<Factura> listaFacturas) {
		this.listaFacturas = listaFacturas;
	}


	public ArrayList<Cliente> getListaClientes() {
		return listaClientes;
	}


	public void setListaClientes(ArrayList<Cliente> listaClientes) {
		this.listaClientes = listaClientes;
	}


	public GestorContabilidad() {
		listaFacturas = new ArrayList<Factura>();
		listaClientes = new ArrayList<Cliente>();
	}
	
	
	public ArrayList<Factura> getListaFacturas() {
		return listaFacturas;
	}
	
	public ArrayList<Cliente> getListaCliente() {
		return listaClientes;
	}
	
	
	
}
